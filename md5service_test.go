package main

import (
	"net/http"
	"net/http/httptest"
	"testing"
	"strings"
)

const DefaultBodyText = "string to md5"


func TestGetMD5Handler(t *testing.T) {
	r := request(t,"/", DefaultBodyText)
	w := httptest.NewRecorder()

	GetMD5Handler(w, r)

	if w.Code != 200 {
		t.Fatalf("Failed: %v", w)
	}

	r2 := request(t,"/", "")
	w2 := httptest.NewRecorder()

	GetMD5Handler(w2, r2)

	if w2.Code != 400 {
		t.Fatalf("Failed: %v", w2)
	}

}

func BenchmarkGetMD5Handler(b *testing.B) {
	b.ReportAllocs()
	b.SetBytes(128)
	b.ResetTimer()
	for i := 1; i < b.N; i++ {
		r := request(b,"/", DefaultBodyText)
		w := httptest.NewRecorder()

		GetMD5Handler(w, r)
	}
}

func request(t testing.TB, url string, body string) *http.Request {
	req, err := http.NewRequest("PUT", url, strings.NewReader(body))
	if err != nil {
		t.Fatal(err)
	}
	return req
}