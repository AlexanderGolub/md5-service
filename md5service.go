package main

import (
	"crypto/md5"
	"encoding/hex"
	"flag"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"strings"
)

const (
	DefaultPort string =  "8080"
)

func GetMD5Handler(w http.ResponseWriter, req *http.Request) {
	w.Header().Set("Content-Type", "text/plain")
	b, err := ioutil.ReadAll(req.Body)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte("Can't read a request."))
		return
	}
	if len(b) == 0 {
		w.WriteHeader(http.StatusBadRequest)
		w.Write([]byte("Got an empty body."))
		return
	}
	h := md5.New()
	io.WriteString(h, string(b))
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(strings.ToUpper(hex.EncodeToString(h.Sum(nil)))))
}

func main() {
	var Port = flag.String("port", DefaultPort, "port to listen")
	flag.Parse()

	http.HandleFunc("/", GetMD5Handler)
	log.Fatal(http.ListenAndServe(":" + *Port, nil))

}
